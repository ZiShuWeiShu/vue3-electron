import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const createRouter = () => new Router({
  routes:[
    {
      path: '/',
      name: 'go-index',
      hidden: false,
      redirect: '/index',
      meta: {
        title: '进入首页'
      }
    },
    {
      path: '/index',
      name: 'index',
      hidden: false,
      meta: {
        title: '首页'
      },
      component: () =>
        import('@/views/page/index.vue'),
    },
  ]
})

export default createRouter